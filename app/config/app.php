<?php

use Cake\Cache\Engine\FileEngine;
use Cake\Cache\Engine\RedisEngine;
use Cake\Database\Connection;
use Cake\Database\Driver\Mysql;
use Cake\Log\Engine\FileLog;
use Cake\Mailer\Transport\SmtpTransport;
use Crud\Error\ExceptionRenderer;

return [
    'debug' => filter_var(env('DEBUG', true), FILTER_VALIDATE_BOOLEAN),
    'App' => [
        'namespace' => 'App',
        'encoding' => env('APP_ENCODING', 'UTF-8'),
        'defaultLocale' => env('APP_DEFAULT_LOCALE', 'en_US'),
        'defaultTimezone' => env('APP_DEFAULT_TIMEZONE', 'UTC'),
        'base' => false,
        'dir' => 'src',
        'webroot' => 'webroot',
        'wwwRoot' => WWW_ROOT,
        //'baseUrl' => env('SCRIPT_NAME'),
        'fullBaseUrl' => false,
        'imageBaseUrl' => 'img/',
        'cssBaseUrl' => 'css/',
        'jsBaseUrl' => 'js/',
        'paths' => [
            'plugins' => [ROOT . DS . 'plugins' . DS],
            'templates' => [APP . 'Template' . DS],
            'locales' => [APP . 'Locale' . DS],
        ],
    ],
    'Cors' => [
        // Accept all origins
        'AllowOrigin' => true,
        'AllowMethods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
    ],
    'Security' => [
        'salt' => env('SECURITY_SALT'),
    ],
    'Asset' => [
        //'timestamp' => true,
        // 'cacheTime' => '+1 year'
    ],
    'Cache' => [
        'default' => [
            'className' => FileEngine::class,
            'path' => CACHE,
            'url' => env('CACHE_DEFAULT_URL', null),
        ],
        '_cake_core_' => [
            'className' => RedisEngine::class,
            'prefix' => 'core_',
            'host' => env('REDIS_IP', '127.0.0.1'),
            'port' => env('REDIS_PORT', 6379),
            'path' => CACHE . 'persistent/',
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKECORE_URL', null),
        ],
        '_cake_model_' => [
            'className' => RedisEngine::class,
            'prefix' => 'model_',
            'host' => env('REDIS_IP', '127.0.0.1'),
            'port' => env('REDIS_PORT', 6379),
            'path' => CACHE . 'models/',
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKEMODEL_URL', null),
        ],
        '_cake_routes_' => [
            'className' => RedisEngine::class,
            'prefix' => 'routes_',
            'host' => env('REDIS_IP', '127.0.0.1'),
            'port' => env('REDIS_PORT', 6379),
            'path' => CACHE,
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKEROUTES_URL', null),
        ],
    ],
    'Error' => [
        'errorLevel' => E_ALL,
        'exceptionRenderer' => ExceptionRenderer::class, //ExceptionRenderer::class,
        'skipLog' => [],
        'log' => true,
        'trace' => true,
    ],
    'EmailTransport' => [
        'default' => [
            'className' => SmtpTransport::class,
            'host' => env('EMAIL_HOST'),
            'port' => env('EMAIL_PORT'),
            'timeout' => 30,
            'username' => env('EMAIL_USER'),
            'password' => env('EMAIL_PASS'),
            'client' => 'api',
            'tls' => (bool)env('EMAIL_TLS', false),
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
        'queue' => [
            'className' => 'Queue.Queue',
            'transport' => 'default',
        ],
    ],
    'Email' => [
        'default' => [
            'transport' => 'queue',
            'from' => ['ecofit2020.app@gmail.com.com' => 'Ecofit'],
            'charset' => 'utf-8',
            'headerCharset' => 'utf-8',
        ],
    ],
    'Datasources' => [
        'default' => [
            'className' => Connection::class,
            'driver' => Mysql::class,
            'persistent' => false,
            'host' => env('DB_HOST'),
            //'port' => 'non_standard_port_number',
            'username' => env('DB_USER'),
            'password' => env('DB_PASS'),
            'database' => env('DB_NAME'),
            //'encoding' => 'utf8mb4',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => true,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
            'url' => env('DATABASE_URL', null),
        ],
    ],
    'Log' => [
        'debug' => [
            'className' => FileLog::class,
            'path' => LOGS,
            'file' => 'debug',
            'url' => env('LOG_DEBUG_URL', null),
            'scopes' => false,
            'levels' => ['notice', 'info', 'debug'],
        ],
        'error' => [
            'className' => FileLog::class,
            'path' => LOGS,
            'file' => 'error',
            'url' => env('LOG_ERROR_URL', null),
            'scopes' => false,
            'levels' => ['warning', 'error', 'critical', 'alert', 'emergency'],
        ],
    ],
    'Session' => [
        'defaults' => 'php',
    ],
];
