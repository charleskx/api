<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {

    $routes->setExtensions(['json']);

    $routes->resources('Classrooms');
    $routes->resources('Modalities');
    $routes->resources('Profiles');
    $routes->resources('Units');
    $routes->resources('Users');

    $routes->resources('Classrooms', function (RouteBuilder $routes) {
        $routes->resources('UserClassrooms', [
            'path' => 'enrollments',
            'prefix' => 'classrooms',
        ]);
    });

    $routes->resources('Users', function (RouteBuilder $routes) {
        $routes->resources('UserClassrooms', [
            'path' => 'enrollments',
            'prefix' => 'users',
        ]);
    });

    $routes->fallbacks(DashedRoute::class);
});
