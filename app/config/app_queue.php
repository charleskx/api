<?php

$execution = MINUTE * env('QUEUE_WORKER_MAX_RUNTIME', 3);

define('WORKER_MAX_RUNTIME', $execution * 10);

return [
    'Queue' => [
        'workermaxruntime' => $execution,
        'defaultworkertimeout' => WORKER_MAX_RUNTIME,
        'cleanuptimeout' => HOUR,
        'multiserver' => env('QUEUE_MULTISERVER', false),
        'sleeptime' => env('QUEUE_WORKER_SLEEPTIME', 6),
        'gcprob' => 10,
	    'maxworkers' => env('QUEUE_MAX_WORKERS', 6),
        'connection' => env('QUEUE_CONNECTION', 'default'),
    ],
];
