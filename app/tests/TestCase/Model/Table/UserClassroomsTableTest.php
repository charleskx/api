<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserClassroomsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserClassroomsTable Test Case
 */
class UserClassroomsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserClassroomsTable
     */
    public $UserClassrooms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserClassrooms',
        'app.Users',
        'app.Classrooms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserClassrooms') ? [] : ['className' => UserClassroomsTable::class];
        $this->UserClassrooms = TableRegistry::getTableLocator()->get('UserClassrooms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserClassrooms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
