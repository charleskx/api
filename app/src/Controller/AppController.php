<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Http\Exception\ForbiddenException;
use Cake\Mailer\MailerAwareTrait;
use Crud\Controller\Component\CrudComponent;
use Crud\Controller\ControllerTrait;
use Exception;
use Muffin\Footprint\Auth\FootprintAwareTrait;

/**
 * Class AppController
 * @package App\Controller\V1
 *
 * @property CrudComponent Crud
 */
class AppController extends Controller
{
    use ControllerTrait;
    use FootprintAwareTrait;
    use MailerAwareTrait;

    /**
     * @throws Exception
     */
    public function initialize()
    {
        parent::initialize();

        // set env for controller
        $_ENV['controller'] = $this->getName();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'Crud.Index',
                'Crud.Add',
                'Crud.View',
                'Crud.Edit',
                'Crud.Delete',
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
                'Crud.Search',
            ],
        ]);

        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password'],
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'userModel' => 'Users',
                    'fields' => [
                        'username' => 'id',
                    ],
                    'parameter' => 'token',
                    'queryDatasource' => true,
                ],
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize',
            'loginAction' => false,
        ]);
    }

    /**
     * @throws Exception
     */
    public function delete()
    {
//        // get controller
//        $controller = $this->getName();
//
//        // start with error
//        $error = true;
//
//        // get user
//        $user = $this->Auth->user();
//        if ($user['type'] === 1 || $controller === 'Accounts') {
//            $error = false;
//        }
//
//        if ($error) {
//            throw new ForbiddenException();
//        }

        // execute
        $this->Crud->execute();
    }

    /**
     * @param int $id
     * @throws ForbiddenException
     */
    final public function security(int $id): void
    {
        // set keys
        $keys = [
            2 => 'client_id',
            3 => 'consultant_id',
        ];

        // get user
        $user = $this->Auth->user();
        if (($type = $user['type']) === 2) {
            // get key
            $key = $keys[$type];

            // get value
            $value = $user[$key] ?? null;
            if ($id === $value) {
                return;
            }

            throw new ForbiddenException();
        }
    }
}
