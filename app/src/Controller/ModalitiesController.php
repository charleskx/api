<?php

namespace App\Controller;

use App\Model\Table\ModalitiesTable;

/**
 * Class UnitsController
 * @package App\Controller
 *
 * @property ModalitiesTable $Modalities
 */
class ModalitiesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index', 'add', 'view', 'edit', 'delete']);
    }
}
