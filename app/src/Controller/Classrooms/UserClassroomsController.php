<?php

namespace App\Controller\Classrooms;

use App\Controller\AppController;
use App\Model\Table\UserClassroomsTable;
use Cake\Event\Event;
use Cake\ORM\Query;
use Exception;

/**
 * Class UnitsController
 * @package App\Controller
 *
 * @property UserClassroomsTable $UserClassrooms
 */
class UserClassroomsController extends AppController
{
    private $classroom_id = 0;

    public function initialize()
    {
        parent::initialize();

        // get user id
        $this->classroom_id = $this->getRequest()->getParam('classroom_id');

        // disable unused actions
        $this->Crud->disable(['add', 'edit', 'view', 'delete']);
    }

    /**
     * @throws Exception
     */
    public function index()
    {
        //enable search
        $this->Crud->addListener('Crud.Search');

        // enable
        $this->Crud->on('beforePaginate', function (Event $event): void {
            /** @var Query $query */
            $query = $event->getSubject()->query;
            $query
                ->where([
                    'classroom_id' => $this->classroom_id
                ])
                ->contain([
                    'Users',
                    'Classrooms' => [
                        'Modalities',
                    ],
                ]);
        });

        // execute
        $this->Crud->execute();
    }
}
