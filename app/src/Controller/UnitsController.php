<?php

namespace App\Controller;

use App\Model\Table\UnitsTable;

/**
 * Class UnitsController
 * @package App\Controller
 *
 * @property UnitsTable $Units
 */
class UnitsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index', 'add', 'view', 'edit', 'delete']);
    }
}
