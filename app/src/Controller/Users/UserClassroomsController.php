<?php

namespace App\Controller\Users;

use App\Controller\AppController;
use App\Model\Entity\UserClassroom;
use App\Model\Table\UserClassroomsTable;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;
use Cake\ORM\Query;
use Exception;

/**
 * Class UnitsController
 * @package App\Controller
 *
 * @property UserClassroomsTable $UserClassrooms
 */
class UserClassroomsController extends AppController
{
    private $user_id = 0;

    public function initialize()
    {
        parent::initialize();

        // get user id
        $this->user_id = $this->getRequest()->getParam('user_id');
    }

    /**
     * @throws Exception
     */
    public function index()
    {
        //enable search
        $this->Crud->addListener('Crud.Search');

        // enable
        $this->Crud->on('beforePaginate', function (Event $event): void {
            /** @var Query $query */
            $query = $event->getSubject()->query;
            $query
                ->where([
                    'user_id' => $this->user_id
                ])
                ->contain([
                    'Classrooms' => [
                        'Modalities',
                    ],
                ]);

            // get weekday
            $modality_id = $this->getRequest()->getQuery('modality_id', 0);
            $weekday = $this->getRequest()->getQuery('weekday', 0);
            if ($modality_id || $weekday) {
                $query
                    ->matching('Classrooms', function (Query $query) use ($modality_id, $weekday) {
                        $query
                            ->select([
                                'id',
                            ]);
                        
                        // change query
                        if ($modality_id) {
                            $query->where(['modality_id' => $modality_id]);
                        }

                        if ($weekday) {
                            $query->where(['weekday' => $weekday]);
                        }

                        // return query
                        return $query;
                    });
            }

        });

        // execute
        $this->Crud->execute();
    }

    /**
     * @throws Exception
     * @throws BadRequestException
     */
    public function add()
    {
        // set type
        $this->Crud->on('beforeSave', function (Event $event) {
            /** @var UserClassroom $entity */
            $entity = $event->getSubject()->entity;

            // get classroom id
            $classroom_id = $this->getRequest()->getData('classroom_id');

            // get classroom
            $classroom = $this->UserClassrooms->Classrooms->get($classroom_id, [
                'fields' => [
                    'capacity',
                    'filled',
                ]
            ]);

            if (($classroom->capacity - $classroom->filled) <= 0) {
                throw new BadRequestException(__('limite de alunos atingido, tente outro horário'));
            }

            // set user type
            $entity->user_id = $this->user_id;
        });

        // execute
        $this->Crud->execute();
    }

    /**
     * @throws Exception
     */
    public function edit()
    {
        // valid type before find
        $this->Crud->on('beforeFind', function (Event $event) {
            /** @var Query $query */
            $query = $event->getSubject()->query;
            $query->where(['user_id' => $this->user_id]);
        });

        // execute
        $this->Crud->execute();
    }

    /**
     * @throws Exception
     */
    public function delete()
    {
        // valid type before find
        $this->Crud->on('beforeFind', function (Event $event) {
            /** @var Query $query */
            $query = $event->getSubject()->query;
            $query->where(['user_id' => $this->user_id]);
        });

        // execute
        $this->Crud->execute();
    }
}
