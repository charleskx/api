<?php

namespace App\Controller;

use App\Model\Table\ProfilesTable;

/**
 * Class ProfilesController
 * @package App\Controller
 *
 * @property ProfilesTable $Profiles
 */
class ProfilesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index', 'add', 'view', 'edit', 'delete']);
    }
}
