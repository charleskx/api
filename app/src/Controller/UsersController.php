<?php

namespace App\Controller;

use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Validation\Validation;
use Exception;
use stdClass;
use Throwable;

/**
 * Users Controller
 *
 * @property UsersTable $Users
 *
 * @method User[]|ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $action = $this->getRequest()->getParam('action');
        if (in_array($action, ['add', 'edit'])) {
            $this->Crud->action()->setConfig('api.success.data.entity', [
                'id',
                'avatar',
            ]);
        }

        $this->Auth->allow(['token', 'recover', 'add', 'mail']);
    }

    /**
     * @throws Exception
     */
    public function index()
    {
        //enable search
        $this->Crud->addListener('Crud.Search');

//        // enable
//        $this->Crud->on('beforePaginate', function (Event $event): void {
//            /** @var Query $query */
//            $query = $event->getSubject()->query;
//
//            $query
//                ->where([
//                    'Users.type' => 1,
//                ]);
//        });

        // execute
        $this->Crud->execute();
    }

    /**
     * @throws Exception
     */
    public function add()
    {
        // after save
        $this->Crud->on('afterSave', function (Event $event) {
            if (!$event->getSubject()->success) {
                return;
            }

            /** @var User $entity */
            $entity = $event->getSubject()->entity;
            $entity = $entity->toArray();

            // send welcome message
            $this->getMailer('User')->send('welcome', [$entity]);
        });


        // execute
        $this->Crud->execute();
    }

    public function mail()
    {
        $entity = $this->Users->get(1);
        $this->getMailer('User')->send('welcome', [$entity]);

        // return data
        $this->set([
            'success' => true,
            '_serialize' => ['success'],
        ]);
    }

    /**
     * @throws Exception
     */
    public function token()
    {
        $this->getRequest()->allowMethod(['post']);

        /** @var stdClass $res */
        $res = $this->getRequest()->input('json_decode');

        if (!isset($res->email) || !isset($res->password)) {
            throw new BadRequestException();
        }

        $user = $this->Auth->identify();

        if (!$user) {
            throw new UnauthorizedException(__('invalid username or password'));
        }

        if (!$user['active']) {
            throw new ForbiddenException(__('disabled user'));
        }

        // create token
        $token = $this->Users->newToken([
            'sub' => $user['id'],
            'iss' => 1,
        ]);

        // return data
        $this->set([
            'success' => true,
            'data' => [
                'user' => $this->me($user['id']),
                'token' => $token,
            ],
            '_serialize' => ['success', 'data'],
        ]);
    }

    /**
     * @param int $id
     * @return User|void
     */
    public function me(int $id = 0)
    {
        $direct = true;
        if (!$id) {
            // get enrollment
            $id = $this->Auth->user('id');

            // set direct
            $direct = false;
        }

        // get user data
        $user = $this->Users->get($id);

        if ($direct) {
            return $user;
        }

        // return data
        $this->set([
            'success' => true,
            'data' => [
                $user,
            ],
            '_serialize' => ['success', 'data'],
        ]);
    }

    /**
     * @throws Exception
     */
    public function recover()
    {
        // allow method
        $this->getRequest()->allowMethod(['post']);

        // get data
        $email = $this->getRequest()->getData('email');

        if (!Validation::email($email)) {
            throw new BadRequestException(__('invalid email'));
        }

        // get user
        try {
            $user = $this
                ->Users
                ->findByEmail($email)
                ->firstOrFail()
                ->toArray();
        } catch (Throwable $e) {
            $user = [];
        }

        // send welcome message
        if ($user) {
            $this->getMailer('User')->send('recover', [$user]);
        }

        // return data
        $this->set([
            'success' => true,
            '_serialize' => ['success'],
        ]);
    }

    public function import()
    {
        // accept only
        $this->getRequest()->allowMethod('post');

        // get file
        $file = $this->getRequest()->getData('file', false);
        if (!$file) {
            throw new BadRequestException(__('invalid file'));
        }

        // open file
        $handle = fopen($file['tmp_name'], 'r');

        // get headers
        $headers = [];

        // get users
        $users = [];

        // iterate
        $first = true;
        while (($line = fgetcsv($handle, 8192, ';')) !== FALSE) {
            if ($first) {
                $headers = $line;
                $first = false;
            } elseif (!empty($line[2])) {
                $users[] = array_combine($headers, $line);
            }
        }

        // get data
        $data = [];

        // save data
        foreach ($users as $user) {
            // make entity
            $entity = $this->Users->newEntity($user);

            try {
                $this->Users->saveOrFail($entity);

                $data[] = [
                    'user' => $entity,
                    'success' => true,
                ];

            } catch (Throwable $e) {
                $data[] = [
                    'user' => $user,
                    'success' => false,
                    'message' => $entity->getErrors()
                ];
            }
        }

        // return data
        $this->set([
            'success' => true,
            'data' => $data,
            '_serialize' => ['success', 'data'],
        ]);
    }
}
