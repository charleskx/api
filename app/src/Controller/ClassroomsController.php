<?php

namespace App\Controller;

use App\Model\Table\ClassroomsTable;
use Cake\Event\Event;
use Cake\ORM\Query;
use Exception;

/**
 * Class UnitsController
 * @package App\Controller
 *
 * @property ClassroomsTable $Classrooms
 */
class ClassroomsController extends AppController
{
    /**
     * @throws Exception
     */
    public function index()
    {
        //enable search
        $this->Crud->addListener('Crud.Search');

        // enable
        $this->Crud->on('beforePaginate', function (Event $event): void {
            /** @var Query $query */
            $query = $event->getSubject()->query;
            $query
                ->contain([
                    'Modalities',
                    'Profiles',
                    'Units',
                ]);

            // get unit
            $unit = $this->getRequest()->getQuery('unit');
            if($unit){
                $query->matching('Units', function(Query $query) use($unit){
                    // change query
                    $query
                        ->select([
                            'id',
                        ])
                        ->where([
                        'Units.name LIKE "%' . $unit . '%"',
                    ]);

                    // return query
                    return $query;
                });
            }

            // get modality
            $modality = $this->getRequest()->getQuery('modality');
            if($modality){
                $query->matching('Modalities', function(Query $query) use($modality){
                    // change query
                    $query
                        ->select([
                            'id',
                        ])
                        ->where([
                            'Modalities.name LIKE "%' . $modality . '%"',
                        ]);

                    // return query
                    return $query;
                });
            }
        });

        // execute
        $this->Crud->execute();
    }

    public function modalities(int $weekday, int $unit_id)
    {
        // get query
        $query = $this->Classrooms->find();
        $query
            ->where([
                'unit_id' => $unit_id,
                'weekday' => $weekday,
            ])
            ->select([
                'unit_id',
                'weekday',
                'modality_id'
            ])
            ->contain([
                'Modalities' => [
                    'fields' => [
                        'id',
                        'name',
                    ]
                ]
            ])
            ->group('modality_id');

        // get data
        $data = $query->toArray();

        // return data
        $this->set([
            'success' => true,
            'data' => $data,
            '_serialize' => ['success', 'data'],
        ]);
    }
}
