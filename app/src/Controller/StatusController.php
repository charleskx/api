<?php

namespace App\Controller;

use Cake\I18n\FrozenTime;

class StatusController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['index']);
    }

    public function index()
    {
        // return data
        $this->set([
            'success' => true,
            'data' => [
                'datetime' => new FrozenTime(),
            ],
            '_serialize' => ['success', 'data'],
        ]);
    }
}
