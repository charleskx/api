<?php

namespace App\Command;

use App\Model\Table\ClassroomsTable;
use App\Model\Table\UserClassroomsTable;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * Cleanup command.
 *
 * @property ClassroomsTable $Classrooms
 * @property UserClassroomsTable $UserClassrooms
 */
class CleanupCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->loadModel('Classrooms');
        $this->loadModel('UserClassrooms');

        $this->UserClassrooms->deleteAll([
            'id !=' => 0,
        ]);

        $this->Classrooms->updateAll([
            'filled' => 0,
        ], [
            'id !=' => 0,
        ]);
    }
}
