<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Unit Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property bool|null $active
 * @property FrozenTime|null $created_at
 * @property FrozenTime|null $modified_at
 *
 * @property User[] $users
 */
class Unit extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        'name' => true,
        'description' => true,
        'active' => true,
        'created_at' => true,
        'modified_at' => true,
        'users' => false,
    ];
}
