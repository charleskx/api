<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Date;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int|null $profile_id
 * @property int|null $unit_id
 * @property string $role
 * @property string $email
 * @property string $password
 * @property string|null $avatar
 * @property string|null $name
 * @property string|null $gender
 * @property Date|null $birthday
 * @property string|null $phone
 * @property string|null $cellphone
 * @property string|null $document
 * @property string|null $identity
 * @property string|null $street
 * @property int|null $number
 * @property string|null $complement
 * @property string|null $locality
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zipcode
 * @property bool $active
 * @property FrozenTime|null $created_at
 * @property FrozenTime|null $modified_at
 * @property FrozenTime|null $deleted_at
 *
 * @property Profile|null $profile
 * @property Unit|null $unit
 */
class User extends Entity
{
    protected $_accessible = [
        'id' => false,
        'profile_id' => true,
        'unit_id' => true,
        'role' => false,
        'email' => true,
        'password' => true,
        'avatar' => true,
        'name' => true,
        'gender' => true,
        'birthday' => true,
        'phone' => true,
        'cellphone' => true,
        'document' => true,
        'identity' => true,
        'street' => true,
        'number' => true,
        'complement' => true,
        'locality' => true,
        'city' => true,
        'state' => true,
        'zipcode' => true,
        'active' => true,
        'created_at' => true,
        'modified_at' => true,
        'deleted_at' => true,
        'profile' => false,
        'unit' => false,
    ];

    protected $_hidden = [
        'password',
    ];

    /**
     * Encrypt password
     * @param string|null $value string for encrypt
     * @return false|string|null
     * @noinspection PhpUnused
     */
    protected function _setPassword(?string $value): ?string
    {
        if (empty($value)) {
            return $value;
        }

        // load hasher
        $hasher = new DefaultPasswordHasher();

        // return string
        return $hasher->hash($value);
    }
}
