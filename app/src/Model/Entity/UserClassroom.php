<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserClassroom Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $classroom_id
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property \Cake\I18n\FrozenTime|null $modified_at
 * @property \Cake\I18n\FrozenTime|null $deleted_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Classroom $classroom
 */
class UserClassroom extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'classroom_id' => true,
        'created_at' => true,
        'modified_at' => true,
        'deleted_at' => true,
        'user' => true,
        'classroom' => true,
    ];
}
