<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Classroom Entity
 *
 * @property int $id
 * @property int $unit_id
 * @property int $modality_id
 * @property int|null $profile_id
 * @property int|null $weekday
 * @property \Cake\I18n\FrozenTime|null $start
 * @property \Cake\I18n\FrozenTime|null $end
 * @property int|null $capacity
 * @property int|null $filled
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property \Cake\I18n\FrozenTime|null $modified_at
 * @property \Cake\I18n\FrozenTime|null $deleted_at
 *
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\Modality $modality
 * @property \App\Model\Entity\Profile $profile
 * @property \App\Model\Entity\UserClassroom[] $user_classrooms
 */
class Classroom extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unit_id' => true,
        'modality_id' => true,
        'profile_id' => true,
        'weekday' => true,
        'start' => true,
        'end' => true,
        'capacity' => true,
        'filled' => true,
        'created_at' => true,
        'modified_at' => true,
        'deleted_at' => true,
        'unit' => true,
        'modality' => true,
        'profile' => true,
        'user_classrooms' => true,
    ];
}
