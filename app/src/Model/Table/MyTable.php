<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MyTable extends Table
{
    protected const KEYS = [
        2 => 'client_id',
        3 => 'consultant_id',
    ];
    protected $isCli = (PHP_SAPI === 'cli');

    public function initialize(array $config): void
    {
        parent::initialize($config);

        // added default behavior
        $this->addBehavior('Default');
    }
}
