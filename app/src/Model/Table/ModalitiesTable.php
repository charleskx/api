<?php

namespace App\Model\Table;

use App\Model\Entity\Modality;
use Cake\Datasource\EntityInterface;
use Cake\Validation\Validator;

/**
 * Modalities Model
 *
 * @method Modality get($primaryKey, $options = [])
 * @method Modality newEntity($data = null, array $options = [])
 * @method Modality[] newEntities(array $data, array $options = [])
 * @method Modality|false save(EntityInterface $entity, $options = [])
 * @method Modality saveOrFail(EntityInterface $entity, $options = [])
 * @method Modality patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Modality[] patchEntities($entities, array $data, array $options = [])
 * @method Modality findOrCreate($search, callable $callback = null, $options = [])
 */
class ModalitiesTable extends MyTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this
            ->setTable('modalities')
            ->setDisplayField('name')
            ->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 16777215)
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
