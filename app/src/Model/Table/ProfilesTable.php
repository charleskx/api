<?php

namespace App\Model\Table;

use App\Model\Entity\Profile;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\Validation\Validator;

/**
 * Profiles Model
 *
 * @property UsersTable&HasMany $Users
 *
 * @method Profile get($primaryKey, $options = [])
 * @method Profile newEntity($data = null, array $options = [])
 * @method Profile[] newEntities(array $data, array $options = [])
 * @method Profile|false save(EntityInterface $entity, $options = [])
 * @method Profile saveOrFail(EntityInterface $entity, $options = [])
 * @method Profile patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Profile[] patchEntities($entities, array $data, array $options = [])
 * @method Profile findOrCreate($search, callable $callback = null, $options = [])
 */
class ProfilesTable extends MyTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this
            ->setTable('profiles')
            ->setDisplayField('name')
            ->setPrimaryKey('id');

        $this->hasMany('Users', [
            'foreignKey' => 'profile_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 16777215)
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
