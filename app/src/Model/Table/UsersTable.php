<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use ArrayObject;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Utility\Security;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use Firebase\JWT\JWT;

/**
 * Users Model
 *
 * @property ProfilesTable&BelongsTo $Profiles
 * @property UnitsTable&BelongsTo $Units
 *
 * @method User get($primaryKey, $options = [])
 * @method User newEntity($data = null, array $options = [])
 * @method User[] newEntities(array $data, array $options = [])
 * @method User|false save(EntityInterface $entity, $options = [])
 * @method User saveOrFail(EntityInterface $entity, $options = [])
 * @method User patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method User[] patchEntities($entities, array $data, array $options = [])
 * @method User findOrCreate($search, callable $callback = null, $options = [])
 * @method Query findByEmail(string $email)
 */
class UsersTable extends MyTable
{
    private $ignore = false;

    public function initialize(array $config): void
    {
        parent::initialize($config);

        // table details
        $this
            ->setTable('users')
            ->setDisplayField('id')
            ->setPrimaryKey('id');

        // behaviors
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'avatar' => [
                'path' => 'webroot{DS}files{DS}{table}{DS}{field}{DS}',
                'nameCallback' => function ($table, $entity, $data) {
                    // get extension
                    $ext = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // return name
                    return Text::uuid() . '.' . $ext;
                }
            ]
        ]);

        // associations
        $this
            ->belongsTo('Profiles')
            ->setForeignKey('profile_id');

        $this
            ->belongsTo('Units')
            ->setForeignKey('unit_id');
    }

    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
//            ->requirePresence('password', 'create')
//            ->notEmptyString('password')
        ;

        return $validator;
    }

    public function beforeFindX(Event $event, Query $query, ArrayObject $options, $primary)
    {
        if ($event->isStopped() || $this->ignore || $this->isCli || !$primary) {
            return;
        }

        /** @var User $user */
        $user = $options['_footprint'] ?? null;
        // apply security
        if ($user && ($type = $user->type) > 1) {
            // get key
            $key = self::KEYS[$type];

            // apply conditions
            $query->where([
                'Users.' . $key => $user[$key],
            ]);
        }
    }

    public function buildRules(RulesChecker $rules)
    {
        $this->ignore = true;

        $rules->add($rules->isUnique(['email'], __('já existe um usuário com este e-mail')));

        $rules->add($rules->existsIn(['profile_id'], 'Profiles'));
        $rules->add($rules->existsIn(['unit_id'], 'Units'));

        return $rules;
    }

    public function newToken(array $config): string
    {
        if (!isset($config['sub'], $config['iss'])) {
            throw new BadRequestException(__('invalid keys to generate token'));
        }

        return JWT::encode([
            'sub' => $config['sub'], //id user
            'iss' => $config['iss'], //id app
        ], Security::getSalt());
    }

    public function checkPassword(int $user_id, string $password): bool
    {
        // get password
        $user = $this->get($user_id);

        // load hasher
        $hasher = new DefaultPasswordHasher();

        // get result
        $check = $hasher->check($password, $user->password);

        // return
        return $check;
    }
}
