<?php

namespace App\Model\Table;

use App\Model\Entity\Unit;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\Validation\Validator;

/**
 * Units Model
 *
 * @property UsersTable&HasMany $Users
 *
 * @method Unit get($primaryKey, $options = [])
 * @method Unit newEntity($data = null, array $options = [])
 * @method Unit[] newEntities(array $data, array $options = [])
 * @method Unit|false save(EntityInterface $entity, $options = [])
 * @method Unit saveOrFail(EntityInterface $entity, $options = [])
 * @method Unit patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Unit[] patchEntities($entities, array $data, array $options = [])
 * @method Unit findOrCreate($search, callable $callback = null, $options = [])
 */
class UnitsTable extends MyTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this
            ->setTable('units')
            ->setDisplayField('name')
            ->setPrimaryKey('id');

        $this
            ->hasMany('classrooms')
            ->setForeignKey('unit_id')
            ->setDependent(true)
            ->setCascadeCallbacks(true);

        $this
            ->hasMany('Users')
            ->setForeignKey('unit_id')
            ->setDependent(true)
            ->setCascadeCallbacks(true);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 16777215)
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
