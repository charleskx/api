<?php

namespace App\Model\Table;

use App\Model\Entity\Classroom;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Classrooms Model
 *
 * @property UnitsTable&BelongsTo $Units
 * @property ModalitiesTable&BelongsTo $Modalities
 * @property ProfilesTable&BelongsTo $Profiles
 * @property UserClassroomsTable&HasMany $UserClassrooms
 *
 * @method Classroom get($primaryKey, $options = [])
 * @method Classroom newEntity($data = null, array $options = [])
 * @method Classroom[] newEntities(array $data, array $options = [])
 * @method Classroom|false save(EntityInterface $entity, $options = [])
 * @method Classroom saveOrFail(EntityInterface $entity, $options = [])
 * @method Classroom patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Classroom[] patchEntities($entities, array $data, array $options = [])
 * @method Classroom findOrCreate($search, callable $callback = null, $options = [])
 */
class ClassroomsTable extends MyTable
{
    // monday ....... sunday
    public $days = [1, 2, 3, 4, 5, 6, 7];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this
            ->setTable('classrooms')
            ->setDisplayField('id')
            ->setPrimaryKey('id');

        $this->belongsTo('Units', [
            'foreignKey' => 'unit_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Modalities', [
            'foreignKey' => 'modality_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Profiles', [
            'foreignKey' => 'profile_id',
        ]);

        $this
            ->hasMany('UserClassrooms')
            ->setForeignKey('classroom_id')
            ->setDependent(true)
            ->setCascadeCallbacks(true);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('unit_id')
            ->requirePresence('unit_id', 'create')
            ->notEmptyString('unit_id');

        $validator
            ->nonNegativeInteger('modality_id')
            ->requirePresence('modality_id', 'create')
            ->notEmptyString('modality_id');

        $validator
            ->nonNegativeInteger('profile_id')
            ->notEmptyString('profile_id');

        $validator
            ->nonNegativeInteger('weekday')
            ->requirePresence('weekday', 'create')
            ->inList('weekday', $this->days)
            ->notEmptyString('weekday');

        $validator
            ->time('start')
            ->requirePresence('start', 'create')
            ->notEmptyTime('start');

        $validator
            ->time('end')
            ->requirePresence('end', 'create')
            ->notEmptyTime('end');

        $validator
            ->nonNegativeInteger('capacity')
            ->requirePresence('capacity', 'create')
            ->notEmptyString('capacity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['unit_id'], 'Units'));
        $rules->add($rules->existsIn(['modality_id'], 'Modalities'));
        $rules->add($rules->existsIn(['profile_id'], 'Profiles'));

        return $rules;
    }
}
