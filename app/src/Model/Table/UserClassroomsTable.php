<?php

namespace App\Model\Table;

use App\Model\Entity\UserClassroom;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * UserClassrooms Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property ClassroomsTable&BelongsTo $Classrooms
 *
 * @method UserClassroom get($primaryKey, $options = [])
 * @method UserClassroom newEntity($data = null, array $options = [])
 * @method UserClassroom[] newEntities(array $data, array $options = [])
 * @method UserClassroom|false save(EntityInterface $entity, $options = [])
 * @method UserClassroom saveOrFail(EntityInterface $entity, $options = [])
 * @method UserClassroom patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method UserClassroom[] patchEntities($entities, array $data, array $options = [])
 * @method UserClassroom findOrCreate($search, callable $callback = null, $options = [])
 */
class UserClassroomsTable extends MyTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this
            ->setTable('user_classrooms')
            ->setDisplayField('id')
            ->setPrimaryKey('id');

        $this->addBehavior('CounterCache', [
            'Classrooms' => ['filled']
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Classrooms', [
            'foreignKey' => 'classroom_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('classroom_id')
            ->requirePresence('classroom_id', 'create')
            ->notEmptyString('classroom_id');

        $validator
            ->nonNegativeInteger('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmptyString('user_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['user_id', 'classroom_id'], __('usuário já esta inscrito')));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['classroom_id'], 'Classrooms'));

        return $rules;
    }
}
