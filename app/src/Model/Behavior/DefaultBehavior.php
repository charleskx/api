<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Search\Model\Behavior\SearchBehavior;

/**
 * Default Behaviors
 *
 * @mixin SearchBehavior
 */
class DefaultBehavior extends Behavior
{
    /**
     * @param array $config default config array
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $table = $this->getTable();

        if (!$table->behaviors()->has('Timestamp')) {
            $table->addBehavior('Timestamp', [
                'events' => [
                    'Model.beforeSave' => [
                        'created_at' => 'new',
                        'modified_at' => 'always',
                    ],
                    'Model.modified' => [
                        'modified_at' => 'always',
                    ],
                ],
            ]);
        }

        // Add the behavior to your table
        $table->addBehavior('Search.Search');
    }
}
