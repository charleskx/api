<?php

declare(strict_types=1);

namespace App\Model\Filter;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Search\Model\Filter\FilterCollection;

/**
 * Class UsersCollection
 * @package App\Model\Filter
 */
class UsersCollection extends FilterCollection
{
    /**
     * Initialize method.
     *
     * @return void
     */
    public function initialize(): void
    {
        // configure search fields
        $this
            ->value('profile_id')
            ->value('unit_id')
            ->value('role')
            ->value('gender')
            ->value('document')
            ->like('name', [
                'before' => true,
                'after' => true
            ]);
    }
}
