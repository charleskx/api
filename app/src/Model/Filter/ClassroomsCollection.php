<?php

declare(strict_types=1);

namespace App\Model\Filter;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Search\Model\Filter\FilterCollection;

/**
 * Class ClassroomsCollection
 * @package App\Model\Filter
 */
class ClassroomsCollection extends FilterCollection
{
    /**
     * Initialize method.
     *
     * @return void
     */
    public function initialize(): void
    {
        // configure search fields
        $this
            ->value('modality_id')
            ->value('weekday')
            ->callback('discount', [
                'callback' => function (Query $query, array $args) {
                    // apply conditions
                    $query->where(function (QueryExpression $exp) {
                        // return expression
                        return $exp->gt('discount', 0);
                    });
                },
            ])
            ->like('name', [
                'before' => true,
                'after' => true
            ]);
    }
}
