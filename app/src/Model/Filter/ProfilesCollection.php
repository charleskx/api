<?php

declare(strict_types=1);

namespace App\Model\Filter;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Search\Model\Filter\FilterCollection;

/**
 * Class ProfilesCollection
 * @package App\Model\Filter
 */
class ProfilesCollection extends FilterCollection
{
    /**
     * Initialize method.
     *
     * @return void
     */
    public function initialize(): void
    {
        // configure search fields
        $this
            ->value('active')
            ->like('name', [
                'before' => true,
                'after' => true
            ]);
    }
}
