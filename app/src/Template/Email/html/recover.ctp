<?php
/**
 * @var int $id
 * @var array $user
 * @var array $people
 * @var string $token
 */

$a = 123;
?>
<tr>
    <td colspan="3" style="padding: 40px 40px 0 40px; background-color: #f9f7f7;">
        <h2 style="color: #736fb0; font-size: 3rem; margin-top: 0; margin-bottom: 0;">Olá Nome!</h2>
        <p>Recebemos uma solicitação para redefinir sua senha. Para continuar clique no botão abaixo.</p>
    </td>
</tr>
<tr>
    <td colspan="3" style="padding: 40px; background-color: #f9f7f7; text-align: center;">
        <a href="https://app.ecofit.com.br/recover/token"
           style="text-decoration: none; color: #FFF; background-color: #e1a631; padding: 15px 30px; border-radius: 30px; font-size: 0.9rem; font-weight: bold; -webkit-box-shadow: 5px 7px 4px -5px rgba(0,0,0,0.75); -moz-box-shadow: 5px 7px 4px -5px rgba(0,0,0,0.75); box-shadow: 5px 7px 4px -5px rgba(0,0,0,0.75);">RECUPERAR SENHA</a>
    </td>
</tr>
