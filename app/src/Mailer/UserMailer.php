<?php

namespace App\Mailer;

use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class UserMailer extends Mailer
{
    public function welcome($entity)
    {
        // set token
//        $entity['token'] = $this->getToken($entity['user']['id'] ?? $entity['id']);

        // send message
        $this
            ->setFrom(['ecofit2020.app@gmail.com' => 'Ecofit'])
            ->setEmailFormat('html')
            ->setTo($entity['email'])
            ->setSubject('Seja bem-vindo à Ecofit')
            ->setViewVars(['data' => $entity])
            ->viewBuilder()
            ->setTemplate('welcome');
    }

    public function recover(array $entity)
    {
        // set token
        $entity['token'] = $this->getToken($entity['user']['id'] ?? $entity['id']);

        // send message
        $this
            ->setFrom(['noreply@sejavalue.com' => 'SejaValue'])
            ->setEmailFormat('html')
            ->setTo($entity['email'])
            ->setSubject('Redefina sua senha Value')
            ->setViewVars(['user' => $entity])
            ->viewBuilder()
            ->setTemplate('recover');
    }

    private function getToken(int $id): string
    {
        // define expires
        $now = new FrozenTime();

        // make token
        $token = JWT::encode([
            'sub' => $id,
            'iss' => 1,
            'iat' => $now,
            'exp' => $now->addHours(12)->toUnixString(),
        ], Security::getSalt());

        // return data
        return $token;
    }
}
