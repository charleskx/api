#!/usr/bin/env bash

./stop.sh

sudo rm -rf vendor/

docker-compose build --force-rm

docker-compose run php su www -c "composer install -o -n --no-scripts"

./start.sh
