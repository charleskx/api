#!/usr/bin/env bash

# get new version from git #
git checkout . && git clean -f && git reset --hard && git pull

cd ./app/ || exit

# execute composer #
composer install --no-dev -n --no-scripts -o

# reset opcache cmd #
php -r 'opcache_reset();'
sudo rm -rf /tmp/opcache/*

# reload services #
sudo service php-fpm reload
sudo service nginx reload

# migrate #
bin/cake migrations migrate -p Queue --no-lock

# migrate default #
#bin/cake migrations migrate --no-lock

# clear cake cache  #
bin/cake cache clear default
bin/cake cache clear _cake_core_
bin/cake cache clear _cake_model_
bin/cake cache clear _cake_routes_

# clear cake orm cache #
bin/cake orm_cache clear